import Album from './Album.jsx';
import useAlbums from '../hooks/useAlbums.js';
export default function AlbumList({ onClick }) {
    console.log("Rendering AlbumList")
    const { albums, isLoading, isError } = useAlbums();

    if (isLoading) {
        return <div id="spinner">Loading...</div>;
    }

    if (isError) {
        return <div className="error">Couldn't load list of albums</div>;
    }
    const albumComponents = albums.map((album) => (
        <Album
            onClick={onClick}
            key={album.id}
            album={album}
        />
    ));

    return <div className="album-list">{albumComponents}</div>;
}
