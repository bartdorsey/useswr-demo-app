export default function Photo({ photo }) {
    return (
        <figure className="photo">
            <img alt="photo" src={photo.thumbnailUrl} />
            <figcaption>{photo.title}</figcaption>
        </figure>
    );
}
