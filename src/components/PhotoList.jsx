import Photo from './Photo';
import usePhotos from '../hooks/usePhotos.js';
export default function PhotoList({ album, backClick }) {
    const { photos, isLoading, isError } = usePhotos(album);

    if (isLoading) {
        return <div id="spinner">Loading...</div>;
    }

    if (isError) {
        return <div className="error">Couldn't load list of dogs</div>;
    }
    const photoComponents = photos.map((photo) => (
        <Photo
            key={photo.id}
            photo={photo}
        />
    ));

    return (
        <div className="photo-list">
            <header>
                <button onClick={backClick}>Back</button>
                <h1>{album.title}</h1>
            </header>
            <div className="photos-container">
                {photoComponents}
            </div>
        </div >
    );
}
