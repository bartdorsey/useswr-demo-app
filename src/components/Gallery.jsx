import { useState } from "react";
import AlbumList from "./AlbumList.jsx";
import PhotoList from "./PhotoList.jsx";
export default function Gallery() {
    const [selectedAlbum, setSelectedAlbum] = useState(null);

    const clearSelectedAlbum = () => {
        setSelectedAlbum(null);
    };

    if (selectedAlbum) {
        return (
            <PhotoList
                album={selectedAlbum}
                backClick={clearSelectedAlbum} />
        );
    }


    return (
        <div className="gallery">
            <AlbumList onClick={setSelectedAlbum} />
        </div>
    );

}
