import CollectionsIcon from '@mui/icons-material/Collections';
export default function Album({ album, onClick }) {
    return (
        <div onClick={e => onClick(album)} className="album">
            <CollectionsIcon />
            <h1>
                {album.title}
            </h1>

        </div>
    );
}
