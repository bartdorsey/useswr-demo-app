import './App.css';
import { SWRConfig } from 'swr';
import Gallery from './components/Gallery.jsx';

function App() {

  return (
    <SWRConfig
      value={{
        fetcher: (resource, init) => fetch(resource, init).then(res => res.json())
      }}
    >
      <div className="App">
        <Gallery />
      </div>
    </SWRConfig>
  );
}

export default App;
