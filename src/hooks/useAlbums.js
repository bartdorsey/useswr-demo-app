import useSWR from 'swr'
export default function useAlbums() {
    const { data, error } = useSWR(
        'https://jsonplaceholder.typicode.com/albums'
    )

    return {
        albums: data,
        isLoading: !(error || data),
        isError: error,
    }
}
