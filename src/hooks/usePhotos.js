import useSWR from "swr";
export default function usePhotos(album) {
  const { data, error } = useSWR(`https://jsonplaceholder.typicode.com/album/${album.id}/photos`);

  return {
    photos: data,
    isLoading: !(error || data),
    isError: error
  };
}
